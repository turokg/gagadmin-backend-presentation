Gaga Admin Project
==================


to run locally:

    $ docker-compose -f local.yml up



then go to:

    http://0.0.0.0:8000/api/

you can obtain your token and then in the "Authorize" popup enter "Bearer {your token}". Then you can try API views

FANCY FEATURES
--------------
* django-cookicutter (docker-compose, project-structure)
* test-coverage!
* migrations by hand
   * to fill initial data
   * to rename model name (removed extra "S")
   * GIN index for full-text search
* commands - to fill db
* swagger API schema
* JWT auth
* email with password restore link is sent to new users created by ADMIN
* elastic-search - for autocomplete suggestions
* django-filters


BOOKING
----------

COMMANDS
^^^^^^^^^^^^

fill some fake reservations:

    $ docker-compose -f local.yml run --rm django python manage.py fill_booking

URLS
^^^^^^^^^^^^

tables querying
""""""""""""""""""""""""
    /api/booking/tables/?max_persons_count=8&start=2021-05-02

filtering fields:

    * start -  accepts date. it will exclude tables which have BOOKED reservations that day
    * max_persons_count


reservations searching example:
""""""""""""""""""""""""""""""""""""""""""""""""
    /api/booking/reservations/?finish__lte=2020-06-15T18:50:08&finish__gte=2020-06-10T18:50:08&search=555&limit=10

filtering fields:

    * is_periodic: exact
    * status: exact
    * start: lte, gte
    * finish: lte, gte


search includes fields:

    * contact_name
    * contact_phone
    * notes


GAMES
----------
COMMANDS
^^^^^^^^^^^^
sync games list with tesera.ru:

    $ docker-compose -f local.yml run --rm django python manage.py sync_tesera_games

rebuild elastic search indexes

    $  docker-compose -f local.yml run --rm django python manage.py search_index --rebuild -f


URLS
^^^^^^^^^^^^

autocompletion example
""""""""""""""""""""""""
    /api/games/suggest/?title__completion=Ма

searching example
""""""""""""""""""""""""
    api/games/?search=magic&playersAgeMin__gte=15&limit=10&offset=0&ordering=id

filtering fields:

    * players_age_min
    * players_max
    * players_max_recommend
    * players_min
    * players_min_recommend
    * playtime_max
    * playtime_min
    * time_to_learn

each filtering field supports queries like contains, gt, gte, lt, lte, range. see more at https://django-elasticsearch-dsl-drf.readthedocs.io/en/latest/filtering_usage_examples.html


ordering fields:

    * players_age_min
    * bgg_geek_rating
    * bgg_rating
    * rating_user
    * n_10rating
    * n_20rating
    * year
    * id



ra
