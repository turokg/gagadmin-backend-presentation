from rest_framework import status
from rest_framework.test import APITestCase

from gaga.users.models import User
from gaga.users.tests.base import BASE_URL

USERNAME = "testusername"
PASSWORD = "3124214412"
WRONG_PASSWORD = "fdlkafasfdlkn"
URL = f"{BASE_URL}auth/token-obtain/"


class TestTokenRetrieval(APITestCase):

    def setUp(self):
        u = User.objects.create(username=USERNAME,
                                email=f"{USERNAME}@mail.ru")
        u.set_password(PASSWORD)
        u.save()

    def test_right_password(self):
        response = self.client.post(URL, {"username": USERNAME,
                                          "password": PASSWORD}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn("access", response.data)

    def test_wrong_password(self):
        response = self.client.post(URL, {"username": USERNAME,
                                          "password": WRONG_PASSWORD},
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
