from copy import copy

from rest_framework import status

from gaga.users.models import User
from gaga.users.tests.base import BASE_URL, \
    REQUIRED_FIELDS, USER_INFO
from gaga.users.factories import UserFactory
from gaga.base_tests import AuthenticatedAPITestCase

URL = BASE_URL


class UserUpdateTest(AuthenticatedAPITestCase):
    def setUp(self):
        super().setUp()
        self.test_user = UserFactory()

    def test_update_user(self):
        response = self.client.put(f"{URL}{self.test_user.username}/",
                                   USER_INFO, format='json')
        self.test_user.refresh_from_db()
        return response


class TestUserUpdateForSuperuser(UserUpdateTest):
    ROLE = User.SUPERUSER

    def test_update_user(self):
        response = super().test_update_user()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        user_info = copy(USER_INFO)
        user_data = user_info.pop("data")
        for key, value in user_info.items():
            if key in ["username"]:
                self.assertNotEqual(value, response.data[key])
                self.assertNotEqual(value, getattr(self.test_user, key))
            else:
                self.assertEqual(value, response.data[key])
                self.assertEqual(value, getattr(self.test_user, key))
        for key, value in user_data.items():
            self.assertEqual(value, response.data["data"][key])
            self.assertEqual(value, getattr(self.test_user.data, key))

    def test_required_field(self):
        for required_field in REQUIRED_FIELDS:
            data = copy(USER_INFO).pop(required_field)
            response = self.client.post(URL, data, format="json")
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class TestUserUpdateForAdmin(UserUpdateTest):
    ROLE = User.ADMIN

    def test_create_user(self):
        response = super().test_update_user()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class TestUserUpdateForStuff(TestUserUpdateForAdmin):
    ROLE = User.STUFF
