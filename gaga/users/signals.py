from django.dispatch import receiver
from django_rest_passwordreset.signals import reset_password_token_created

from gaga.utils.mail import send_invite_msg


@receiver(reset_password_token_created)
def password_reset_token_created(sender, instance, reset_password_token, *args, **kwargs):
    send_invite_msg(reset_password_token)
