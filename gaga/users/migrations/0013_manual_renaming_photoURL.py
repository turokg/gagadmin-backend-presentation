from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('users', '0012_auto_20200530_1324'),
    ]

    operations = [
        migrations.RenameField(
            model_name='userdata',
            old_name='photoURL',
            new_name='photo_utl',
        ),
    ]
