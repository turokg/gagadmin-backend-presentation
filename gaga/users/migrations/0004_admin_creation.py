from django.contrib.auth.hashers import make_password
from django.db import migrations

ADMIN_DATA = {
    "role": "superuser",
    "data": {
        "display_name": "Arnold Matlock",
        "photo_url": None,
        "email": "staff@mail.com",
        "settings": {
            "layout": {
                "style": "layout2",
                "config": {
                    "mode": "boxed",
                    "scroll": "content",
                    "navbar": {
                        "display": True
                    },
                    "toolbar": {
                        "display": True,
                        "position": "below"
                    },
                    "footer": {
                        "display": True,
                        "style": "fixed"
                    }
                }
            },
            "custom_scrollbars": True,
            "theme": {
                "main": "greeny",
                "navbar": "mainThemeDark",
                "toolbar": "mainThemeDark",
                "footer": "mainThemeDark"
            }
        },
        "shortcuts": [
            "calendar",
            "mail",
            "contacts",
            "todo"
        ]
    }
}


def add_items(apps, schema_editor):
    User = apps.get_model("users", "User")
    UserData = apps.get_model("users", "UserData")
    data = UserData.objects.create(**ADMIN_DATA["data"])
    User.objects.filter(username="admin").delete()
    User.objects.create(username="admin", email="adin@mail.com", password=make_password("pass"), is_superuser=True, role=ADMIN_DATA["role"], data=data)


class Migration(migrations.Migration):
    dependencies = [("users", "0003_auto_20200525_1700")]
    operations = [migrations.RunPython(add_items)]
