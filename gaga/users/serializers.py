from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from gaga.users.models import User, UserData


class UserDataSerializer(serializers.ModelSerializer):
    shortcuts = serializers.ListField(child=serializers.CharField(),
                                      allow_empty=True)

    class Meta:
        model = UserData
        fields = "__all__"


class UserSerializer(serializers.ModelSerializer):
    data = UserDataSerializer()
    is_active = serializers.BooleanField(required=True)
    email = serializers.EmailField(required=True, validators=[
        UniqueValidator(queryset=User.objects.all()), ])

    class Meta:
        model = User
        fields = ["username", "role", "data", "is_active", "email"]

    def create(self, validated_data):
        """
        password is left empty
        generate password and send it by email ??
        """
        user_data = UserData.objects.create(**validated_data.pop("data", {}))
        return User.objects.create(**validated_data, data=user_data)

    def update(self, instance: User, validated_data: dict):
        """
        updates only user data
        other fields like username, role are ignored
        """
        for attr_name in ("is_active", "email", "role"):
            if attr_name in validated_data:
                setattr(instance, attr_name, validated_data[attr_name])
        instance.save()

        user_data = instance.data
        new_user_data = validated_data.pop("data", {})
        if user_data:
            for name, value in new_user_data.items():
                setattr(user_data, name, value)
            user_data.save()
        else:
            instance.data = UserData.objects.create(**new_user_data)
            instance.save()
        return instance
