from rest_framework.test import APITestCase
from rest_framework_simplejwt.tokens import AccessToken

from gaga.users.models import User



class AuthenticatedAPITestCase(APITestCase):
    ROLE = User.ADMIN

    def setUp(self):
        self.user = User.objects.create(username=f"{self.ROLE}_name",
                                   role=self.ROLE, email=f"{self.ROLE}")
        self.client.credentials(
            HTTP_AUTHORIZATION=f'Bearer {AccessToken.for_user(self.user)}')
