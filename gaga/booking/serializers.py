from rest_framework import serializers

from gaga.booking.models import Reservation, Room, Table


class RoomSerializer(serializers.ModelSerializer):
    class Meta:
        model = Room
        fields = "__all__"


class TableSerializer(serializers.ModelSerializer):
    room = serializers.SlugRelatedField(read_only=False, slug_field="name",
                                        queryset=Room.objects.all())

    class Meta:
        model = Table
        fields = "__all__"


class ReservationSerializer(serializers.ModelSerializer):
    tables = serializers.SlugRelatedField(read_only=False, slug_field="name",
                                          many=True,
                                          queryset=Table.objects.all())

    class Meta:
        model = Reservation
        fields = "__all__"
