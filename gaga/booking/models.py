from django.db import models


class Room(models.Model):
    name = models.CharField(max_length=100, unique=True)


class Table(models.Model):
    name = models.CharField(max_length=100, unique=True)
    max_persons_count = models.PositiveSmallIntegerField()
    room = models.ForeignKey(Room, related_name="tables",
                             on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return f"{self.name}"


class Reservation(models.Model):
    BOOKED = "booked"
    CANCELLED = "cancelled"
    SUCCESS = "success"
    STATUS_CHOICES = [
        (BOOKED, "Booked"),
        (CANCELLED, "Cancelled"),
        (SUCCESS, "Success")
    ]
    contact_name = models.CharField(max_length=200)
    contact_phone = models.CharField(max_length=100, null=True, blank=True)
    notes = models.TextField(null=True, blank=True)
    status = models.CharField(max_length=9, choices=STATUS_CHOICES,
                              default=BOOKED, blank=True)
    start = models.DateTimeField()
    finish = models.DateTimeField(null=True, blank=True)
    is_periodic = models.BooleanField(default=False)
    tables = models.ManyToManyField(Table, related_name="reservations")

    def __repr__(self):
        return f"{self.start} - {self.finish}"

    class Meta:
        ordering = ["start"]
        indexes = (
            models.Index(fields=["status"]),
            models.Index(fields=["start"]),
            models.Index(fields=["finish"]),
            models.Index(fields=["is_periodic"]),

        )

    def __str__(self):
        return (f"{self.start} - {self.finish} of tables "
                f"[{','.join(map(str, self.tables.all()))}] "
                f"({self.contact_name, self.contact_phone})")

    # TODO indexes: status, all text fields, start, finish, is_periodic
    # TODO if status BOOKED -> SUCCESS set finish_date
