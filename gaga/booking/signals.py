from typing import Iterable

from django.db.models.signals import m2m_changed, pre_save
from django.utils import timezone
from rest_framework.exceptions import ValidationError

from gaga.booking.models import Reservation, Table


def check_start_finish(sender, instance, *args, **kwargs):
    if instance.finish:
        if instance.start >= instance.finish:
            raise ValidationError({"finish": "start must be less than finish"})


pre_save.connect(check_start_finish, sender=Reservation)


def check_previous_reservations(reservation: Reservation,
                                table_ids: Iterable[int]):
    for table_id in table_ids:
        table = Table.objects.get(id=table_id)
        start = reservation.start
        finish = reservation.finish or timezone.now().replace(hour=23,
                                                              minute=59)
        previous_reservations = (table.reservations
                                 .exclude(id=reservation.id)
                                 .filter(start__lt=finish,
                                         status=Reservation.BOOKED))
        no_finish = previous_reservations.filter(finish__isnull=True,
                                                 start__day=start.day)
        has_finish = previous_reservations.filter(finish__gt=start)
        if no_finish or has_finish:
            if not reservation.tables.all():
                reservation.delete()
            raise ValidationError(
                {"tables": f"Table {table.name} is already taken"})


def adding_tables_check(sender, instance: Reservation, action, reverse, model,
                        pk_set, *args, **kwargs):
    if not reverse and action == "pre_add":
        check_previous_reservations(instance, pk_set)


m2m_changed.connect(adding_tables_check,
                    sender=Reservation.tables.through)


def changing_attrs_check(sender, instance: Reservation, *args, **kwargs):
    if instance.id:
        check_previous_reservations(instance,
                                    instance.tables.all().values_list("id",
                                                                      flat=True))


pre_save.connect(changing_attrs_check, sender=Reservation)
