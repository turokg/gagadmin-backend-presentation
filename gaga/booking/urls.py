from rest_framework.routers import SimpleRouter

from gaga.booking.views import ReservationViewSet, RoomViewSet, TableViewSet

router = SimpleRouter()
router.register("tables", TableViewSet)
router.register("rooms", RoomViewSet)
router.register("reservations", ReservationViewSet)
urlpatterns = router.urls
