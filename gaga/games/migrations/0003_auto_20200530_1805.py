# Generated by Django 3.0.5 on 2020-05-30 15:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0002_auto_20200530_1803'),
    ]

    operations = [
        migrations.AddField(
            model_name='game',
            name='title2',
            field=models.CharField(blank=True, max_length=300, null=True),
        ),
        migrations.AddField(
            model_name='game',
            name='title3',
            field=models.CharField(blank=True, max_length=300, null=True),
        ),
    ]
