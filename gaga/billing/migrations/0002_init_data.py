from django.db import migrations

CONSTANTS = [
    {
        "name": "TOTAL_MAX",
        "value": 650
    },
    {
        "name": "DEFAULT_PER_HOUR_COST",
        "value": 2
    }
]

TIME_RATES = [
    {
        "cost": 2,
        "start_hour": 12,
        "finish_hour": 18,
        "days_of_week": [1, 2, 3, 4, 5]
    },
    {
        "cost": 2.5,
        "start_hour": 18,
        "finish_hour": 23,
        "days_of_week": [1, 2, 3, 4, 5]
    },
    {
        "cost": 2.5,
        "start_hour": 12,
        "finish_hour": 24,
        "days_of_week": [1, 2, 3, 4, 5, 6, 7]
    }
]

FIXED_RATES = [
    {
        "name": "Ночь выходных: лайт",
        "cost": 350
    },
    {
        "name": "Ночь выходных",
        "cost": 450
    }
]


def create_constants(apps, schema_editor):
    Constant = apps.get_model('billing', 'Constant')
    Constant.objects.bulk_create([Constant(**c) for c in CONSTANTS])


def create_time_rates(apps, schema_editor):
    TimeRate = apps.get_model('billing', 'TimeRate')
    TimeRate.objects.bulk_create([TimeRate(**r) for r in TIME_RATES])


def create_fixed_rates(apps, schema_editor):
    FixedRate = apps.get_model('billing', 'FixedRate')
    FixedRate.objects.bulk_create([FixedRate(**r) for r in FIXED_RATES])


class Migration(migrations.Migration):
    dependencies = [('billing', '0001_initial')]
    operations = [
        migrations.RenameModel('Constants', 'Constant'),
        migrations.RunPython(create_constants),
        migrations.RunPython(create_time_rates),
        migrations.RunPython(create_fixed_rates),
    ]
