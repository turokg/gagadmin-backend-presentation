from rest_framework import serializers

from gaga.billing.models import Bill, Constant, FixedRate, Guest, TimeRate, \
    Visit


class TimeRateSerializer(serializers.ModelSerializer):
    class Meta:
        model = TimeRate
        fields = "__all__"


class FixedRateSerializer(serializers.ModelSerializer):
    class Meta:
        model = FixedRate
        fields = "__all__"


class GuestSerializer(serializers.ModelSerializer):
    class Meta:
        model = Guest
        fields = "__all__"


class BillSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bill
        fields = "__all__"


class ConstantSerializer(serializers.ModelSerializer):
    value = serializers.FloatField()

    class Meta:
        model = Constant
        fields = ("value",)


class CustomSlugRelatedField(serializers.SlugRelatedField):
    def to_internal_value(self, data):
        try:
            obj, _ = self.get_queryset().get_or_create(**{self.slug_field: data})
            return obj
        except (TypeError, ValueError):
            self.fail('invalid')


class VisitSerializer(serializers.ModelSerializer):
    guest = CustomSlugRelatedField(read_only=False,
                                         slug_field="card_number",
                                         queryset=Guest.objects.all())
    bonus_receiver = CustomSlugRelatedField(read_only=False,
                                                  slug_field="card_number",
                                                  queryset=Guest.objects.all(),
                                                  required=False)
    fixed_rate = serializers.SlugRelatedField(read_only=False,
                                              slug_field="name",
                                              queryset=FixedRate.objects.all(),
                                              required=False)
    start = serializers.DateTimeField(required=True)
    finish = serializers.DateTimeField(required=False)
    bill = BillSerializer(read_only=True)

    class Meta:
        model = Visit
        fields = "__all__"

    # def create(self, validated_data):
    #     """
    #     creation of guests if they are not present in the system
    #     """
    #     guest_card_number = validated_data.get("guest")
    #     Guest.objects.get_or_create(card_number=guest_card_number)
    #     bonus_card_number = validated_data.get("bonus_receiver")
    #     if bonus_card_number:
    #         Guest.objects.get_or_create(card_number=bonus_card_number)
    #     super().create(validated_data)
