from datetime import timedelta
from random import randint

from django.utils import timezone
from faker import Faker
from rest_framework import status

from gaga.base_tests import AuthenticatedAPITestCase
from gaga.billing.models import Guest, Visit
from gaga.users.models import User

URL = "/api/billing/visits/today/"
VISITS_COUNT = 10
FAKE = Faker()


class TestEmptyToday(AuthenticatedAPITestCase):
    ROLE = User.SUPERUSER

    def test_today(self):
        response = self.client.get(URL)
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertFalse(response.data)


class TestToday(AuthenticatedAPITestCase):
    ROLE = User.ADMIN

    def setUp(self):
        for _ in range(VISITS_COUNT):
            start = timezone.now().replace(hour=randint(12, 23))
            finish = start + timedelta(minutes=randint(5, 5 * 60))
            Visit.objects.create(
                guest=Guest.objects.create(card_number=FAKE.user_name()),
                start=start, finish=finish)

    def test_today(self):
        response = self.client.get(URL)
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(VISITS_COUNT, len(response.data))
