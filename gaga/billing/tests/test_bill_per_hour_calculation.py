from datetime import datetime, timedelta

import pytest
from django.test import TestCase

from gaga.billing.models import Guest, Visit

START_FRIDAY = datetime(2020, 12, 30, 13, 00)
START_SUNDAY = datetime(2020, 12, 27, 13, 00)
DURATION = timedelta(hours=4)
CARD_NUMBER = "fdsaljhjafsd"


class TestBillFridayMorning(TestCase):
    START = START_FRIDAY
    FINISH = START + DURATION
    EXPECTED_TOTAL = 2 * DURATION.total_seconds() / 60
    FINISH_AFTER_CHANGE = START + DURATION / 2
    EXPECTED_AFTER_CHANGE = EXPECTED_TOTAL / 2

    def setUp(self) -> None:
        self.visit = Visit.objects.create(
            guest=Guest.objects.create(card_number=CARD_NUMBER),
            start=self.START,
            finish=self.FINISH)

    def test_bill_creation(self):
        self.assertEqual(self.visit.start, self.START)
        self.assertEqual(self.visit.finish, self.FINISH)
        self.assertEqual(self.visit.fixed_rate, None)

    def test_bill(self):
        self.assertEqual(self.visit.bill.total, self.EXPECTED_TOTAL)

    def test_bill_after_change(self):
        self.visit.finish = self.FINISH_AFTER_CHANGE
        self.visit.save()
        self.visit.refresh_from_db()
        self.assertEqual(self.visit.bill.total, self.EXPECTED_AFTER_CHANGE)


class TestBillFridayEvening(TestBillFridayMorning):
    START = START_FRIDAY.replace(hour=18)
    FINISH = START + DURATION
    EXPECTED_TOTAL = 2.5 * DURATION.total_seconds() / 60
    FINISH_AFTER_CHANGE = START + DURATION / 2
    EXPECTED_AFTER_CHANGE = EXPECTED_TOTAL / 2


class TestBillFridayBorder(TestBillFridayMorning):
    START = START_FRIDAY.replace(hour=16)
    FINISH = START + DURATION
    EXPECTED_TOTAL = 2.5 * 120 + 2 * 120
    FINISH_AFTER_CHANGE = START + DURATION / 2
    EXPECTED_AFTER_CHANGE = 2 * 120


class TestBillSundayBorder(TestBillFridayMorning):
    START = START_SUNDAY
    FINISH = START + DURATION
    EXPECTED_TOTAL = 2.5 * DURATION.total_seconds() / 60
    FINISH_AFTER_CHANGE = START + DURATION / 2
    EXPECTED_AFTER_CHANGE = EXPECTED_TOTAL / 2


class TestBillFridayNotRound(TestBillFridayMorning):
    START = START_FRIDAY.replace(hour=13, minute=13)
    FINISH = START.replace(hour=15, minute=34)
    EXPECTED_TOTAL = 2 * (FINISH - START).total_seconds() / 60
    FINISH_AFTER_CHANGE = START.replace(hour=17, minute=10)
    EXPECTED_AFTER_CHANGE = 2 * (
        FINISH_AFTER_CHANGE - START).total_seconds() / 60


@pytest.mark.transactional_db
class TestMaxLimit(TestBillFridayMorning):
    START = START_FRIDAY.replace(hour=12, minute=0)
    FINISH = START.replace(hour=23, minute=0)
    EXPECTED_TOTAL = 650
    FINISH_AFTER_CHANGE = START.replace(hour=13)
    EXPECTED_AFTER_CHANGE = 2 * (
        FINISH_AFTER_CHANGE - START).total_seconds() / 60
