from rest_framework import status

from gaga.base_tests import AuthenticatedAPITestCase
from gaga.billing.models import TimeRate
from gaga.users.models import User

URL = "/api/billing/time-rates/total-max/"


class TestTotalMaxSuperuser(AuthenticatedAPITestCase):
    ROLE = User.SUPERUSER

    def test_retrieval(self):
        response = self.client.get(URL)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["value"], TimeRate.total_max)

    def test_update(self):
        new_value = 1000
        response = self.client.put(URL, {"value": new_value}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["value"], new_value)
        self.assertEqual(TimeRate.total_max, new_value)


class TestTotalMaxAdmin(TestTotalMaxSuperuser):
    ROLE = User.ADMIN


class TestTotalMaxStuff(TestTotalMaxSuperuser):
    ROLE = User.STUFF

    def test_update(self):
        new_value = 1000
        response = self.client.put(URL, {"value": new_value}, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
