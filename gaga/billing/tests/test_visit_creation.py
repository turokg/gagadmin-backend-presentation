from copy import copy
from datetime import datetime, timedelta

from rest_framework import status

from gaga.base_tests import AuthenticatedAPITestCase
from gaga.billing.models import Guest
from gaga.users.models import User

URL = "/api/billing/visits/"

START = datetime(2020, 12, 30, 11, 15)
FINISH = START + timedelta(hours=4)
VISIT_DATA = {
    "guest": "31289312132",
    "bonus_receiver": "31289312132",
    "fixed_rate": "Ночь выходных",
    "start": START.isoformat(),
    "finish": FINISH.isoformat()
}


class TestVisitCreationAdmin(AuthenticatedAPITestCase):
    ROLE = User.STUFF

    def test_visit_retrieval(self):
        response = self.client.get(URL)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_creation(self):
        response = self.client.post(URL, VISIT_DATA, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class TestVisitCreationNoFinish(TestVisitCreationAdmin):
    ROLE = User.SUPERUSER

    def test_creation(self):
        data = copy(VISIT_DATA)
        data.pop("finish")
        response = self.client.post(URL, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class TestVisitCreationNoBonusReceiver(TestVisitCreationAdmin):
    ROLE = User.SUPERUSER

    def test_creation(self):
        data = copy(VISIT_DATA)
        data.pop("bonus_receiver")
        response = self.client.post(URL, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class TestVisitCreationSuperuser(TestVisitCreationAdmin):
    ROLE = User.SUPERUSER

    def test_creation(self):
        response = self.client.post(URL, VISIT_DATA, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(Guest.objects.filter(card_number=VISIT_DATA["guest"]))
        self.assertEqual(response.data["bill"]["details"]["rate_type"], "fixed")

    def test_creation_no_bonus_receiver(self):
        data = copy(VISIT_DATA)
        data.pop("bonus_receiver")
        response = self.client.post(URL, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data["guest"],
                         response.data["bonus_receiver"])

    def test_creation_other_bonus_receiver(self):
        data = copy(VISIT_DATA)
        bonus_receiver_card = "vzlsijnfasdcjn"
        data.update(bonus_receiver=bonus_receiver_card)
        response = self.client.post(URL, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertNotEqual(response.data["guest"],
                            response.data["bonus_receiver"])
        self.assertEqual(response.data["bonus_receiver"], bonus_receiver_card)


class TestVisitCreationAdmin(TestVisitCreationSuperuser):
    ROLE = User.ADMIN


class TestVisitCreationPerMinute(TestVisitCreationAdmin):
    def test_creation(self):
        data = copy(VISIT_DATA)
        data.pop("fixed_rate")
        response = self.client.post(URL, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(Guest.objects.filter(card_number=data["guest"]))
        self.assertEqual(response.data["bill"]["details"]["rate_type"],
                         "per_minute")


class TestVisitCreationWrong(TestVisitCreationAdmin):

    def test_creation(self):
        data = copy(VISIT_DATA)
        data.update(start=data["finish"], finish=data["start"])
        response = self.client.post(URL, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
