from django.conf import settings
from django.conf.urls import url
from django.contrib import admin
from django.urls import include, path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

urlpatterns = []

# API URLS
api_patterns = [
    path("users/", include("gaga.users.urls")),
    path("games/", include("gaga.games.urls")),
    path("booking/", include("gaga.booking.urls")),
    path("billing/", include("gaga.billing.urls")),
]

urlpatterns += [path("api/", include(api_patterns))]

if settings.DEBUG:
    schema_view = get_schema_view(
        openapi.Info(
            title="GAGA admin API",
            default_version='v1',
            description="TODO: do we need any description?",
            contact=openapi.Contact(email="turokg@icloud.com"),
            license=openapi.License(name="BSD License"),
        ),
        # url="api",
        public=True,
        permission_classes=(permissions.AllowAny,),
        patterns=urlpatterns
    )
    urlpatterns += [url(r'^api/', schema_view.with_ui('swagger',
                                                      cache_timeout=0),
                        name='schema-swagger-ui'), ]
    urlpatterns += [path(settings.ADMIN_URL, admin.site.urls), ]
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.

    if "debug_toolbar" in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns = [path("__debug__/",
                            include(debug_toolbar.urls))] + urlpatterns
